<?php

namespace Drupal\file_history\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an widget with memory of uploaded file.
 *
 * @FormElement("file_history")
 */
class FileHistory extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processFileHistory'],
      ],
      '#theme_wrappers' => ['form_element'],
      '#progress_indicator' => 'throbber',
      '#progress_message' => NULL,
      '#upload_validators' => [],
      '#upload_location' => NULL,
      '#size' => 22,
      '#multiple' => FALSE,
      '#extended' => FALSE,
      '#attached' => [
        'library' => ['file/drupal.file'],
      ],
      '#element_validate' => [
        [$class, 'validateFileHistory'],
      ],
      '#accept' => NULL,
      '#content_validator' => [],

      // Manage options.
      '#no_upload' => FALSE,
      '#no_use' => FALSE,
      '#no_download' => FALSE,

      // Legacy mode.
      '#legacy' => FALSE,
      '#create_missing' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {

    // If no upload_location, no field.
    if ($element['#upload_location'] == NULL) {
      \Drupal::messenger()->addError("'#upload_location' attribute are mandatory in file_history definition.");
      return;
    }

    $new_files = [];
    // If there is input.
    if ($input !== FALSE) {
      // Uploads take priority over all other values.
      if ($files = file_managed_file_save_upload($element, $form_state)) {
        $new_files = array_keys($files);
      }
    }

    // Prepare default values.
    $values = self::makeValues($element, $form_state, $new_files);

    // Intercept action on row button.
    $button = $form_state->getTriggeringElement();

    if (strstr($button["#name"], $element['#name'])) {

      // Manage Select/deselect.
      if (strstr($button["#name"], 'select_file_button_')) {
        $is_multiple = ($element['#cardinality'] > 1 || $element['#cardinality'] == -1);
        self::manageSelected($button["#name"], $values, $is_multiple);
      }

      // Manage Delete.
      if (strstr($button["#name"], 'delete_button_')) {
        self::deleteFile($button["#name"], $values);
      }
    }

    // Return default selected value.
    return $values;
  }

  /**
   * Ajax callback for file_history upload forms.
   *
   * @param mixed $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response of the ajax upload.
   */
  public static function uploadAjaxCallback(&$form, FormStateInterface &$form_state, Request $request) {
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');

    $form_parents = explode('/', $request->query->get('element_parents'));

    // Sanitize form parents before using them.
    $form_parents = array_filter($form_parents, [Element::class, 'child']);

    // Retrieve the element to be rendered.
    $form = NestedArray::getValue($form, $form_parents);

    $form['#suffix'] = (isset($form['#suffix'])) ? $form['#suffix'] : '';
    // Add the special AJAX class if a new file was added.
    $current_file_count = $form_state->get('file_upload_delta_initial');
    if (isset($form['#file_upload_delta']) && $current_file_count < $form['#file_upload_delta']) {
      $form[$current_file_count]['#attributes']['class'][] = 'ajax-new-content';
    }
    // Otherwise just add the new content class on a placeholder.
    else {
      $form['#suffix'] .= '<span class="ajax-new-content"></span>';
    }

    $status_messages = ['#type' => 'status_messages'];
    if (!isset($form['#prefix'])) {
      $form['#prefix'] = "";
    }
    $form['#prefix'] .= $renderer->renderRoot($status_messages);
    $output = $renderer->renderRoot($form);
    $response = new AjaxResponse();
    $response->setAttachments($form['#attached']);

    return $response->addCommand(new ReplaceCommand(NULL, $output));
  }

  /**
   * Render API callback: Expands the managed_file element type.
   *
   * Expands the file type to include Upload and Remove buttons, as well as
   * support for a default value.
   */
  public static function processFileHistory(&$element, FormStateInterface $form_state, &$complete_form) {
    // If no upload_location, no field.
    if ($element['#upload_location'] == NULL) {
      return;
    }

    // Determine the number of widgets to display.
    $element['#cardinality'] = (isset($element['#cardinality'])) ? $element['#cardinality'] : 1;
    $element['#multiple'] = TRUE;

    $no_upload = (isset($element['#no_upload']) && $element['#no_upload'] === TRUE);
    $no_download = (isset($element['#no_download']) && $element['#no_download'] === TRUE);
    $no_use = (isset($element['#no_use']) && $element['#no_use'] === TRUE);

    // Prepare upload fields.
    // This is used sometimes so let's implode it just once.
    $parents_prefix = implode('_', $element['#parents']);
    $element['#tree'] = TRUE;

    // Activate ajax only if the form element declared an wrapper id.
    $ajax_activated = FALSE;
    $ajax_settings = [];
    if (isset($element['#theme_wrappers']['form_element']['#wrapper_attributes']['id'])) {
      $ajax_wrapper_id = $element['#theme_wrappers']['form_element']['#wrapper_attributes']['id'];
      $ajax_settings = [
        'callback' => [get_called_class(), 'uploadAjaxCallback'],
        'options' => [
          'query' => [
            'element_parents' => implode('/', $element['#array_parents']),
          ],
        ],
        'wrapper' => $ajax_wrapper_id,
        'effect' => 'fade',
        'progress' => [
          'type' => $element['#progress_indicator'],
          'message' => $element['#progress_message'],
        ],
      ];
      $ajax_activated = TRUE;
    }

    $extension_list = '';
    // Add the extension list to the page as JavaScript settings.
    if (isset($element['#upload_validators']['file_validate_extensions'][0])) {
      $extension_list = implode(',', array_filter(explode(' ', $element['#upload_validators']['file_validate_extensions'][0])));
    }

    if (!$no_upload) {
      // Add Upload field.
      $element['upload'] = [
        '#name' => 'files[' . $parents_prefix . ']',
        '#type' => 'file',
        '#title' => t('Choose a file'),
        '#title_display' => 'invisible',
        '#size' => $element['#size'],
        '#multiple' => $element['#multiple'],
        '#theme_wrappers' => [],
        '#weight' => -10,
        '#error_no_message' => TRUE,
        '#attached' => ['drupalSettings' => ['file' => ['elements' => ['#' . $element['#id'] => $extension_list]]]],
      ];

      // Add upload button.
      $element[$parents_prefix . '_upload_button'] = [
        '#name' => $parents_prefix . '_upload_button',
        '#type' => 'submit',
        '#value' => t('Upload'),
        // '#attributes' => ['class' => ['js-hide']],
        '#validate' => [],
        '#submit' => ['file_history_submit'],
        '#limit_validation_errors' => [],
        '#weight' => -5,
      ];

      if ($ajax_activated === TRUE) {
        $element[$parents_prefix . '_upload_button']['#ajax'] = $ajax_settings;
      }
    }

    // Add Table Header.
    $header = [
      'fid' => '',
      'filename' => t('Filename'),
      'weight' => t('Weight'),
      'uploaded' => t('Uploaded at'),
      'activ' => t('Is active file ?'),
      'operation' => t('Operations'),
      'selected' => '',
    ];

    if ($no_use) {
      unset($header['activ']);
      unset($header['selected']);
    }

    // Wait alterations of headers.
    $field_id = isset($element['#field_general_id_for_hooks']) ? $element['#field_general_id_for_hooks'] : $element['#name'];
    // Generic alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_headers_alter',
      [&$header, $field_id]
    );
    // Specific alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_' . $field_id . '_headers_alter',
      [&$header]
    );

    // Prepare table rows.
    $rows = [];

    $files_id = (isset($element['#value']['in_table'])) ? $element['#value']['in_table'] : [];
    $selected_ids = (isset($element['#value']['selected'])) ? $element['#value']['selected'] : [];

    if ($element['#cardinality'] == -1 || $element['#cardinality'] == 1) {
      // If One, the manage_selected method provide a "more easy" behavior.
      $more_available = TRUE;
    }
    else {
      $more_available = (count($selected_ids) < $element['#cardinality']);
    }

    // For Each files.
    foreach ($files_id as $fid) {

      $add_selection_button = TRUE;
      $fObj = File::load($fid);

      // If the file aren't know by Drupal.
      if ($fObj == NULL) {
        // Pass to next file.
        continue;
      }

      // Process File for Table.
      $fid = $fObj->id();
      $realpath = \Drupal::service('file_system')->realpath($fObj->getFileUri());
      $fileRow = [];
      $fileRow['fid'] = [
        '#type' => 'hidden',
        '#value' => $fid,
      ];
      $fileRow['filename'] = ['#markup' => $fObj->getFilename()];
      $fileRow['weight'] = ['#markup' => format_size(filesize($realpath))];
      $fileRow['uploaded'] = ['#markup' => date('Y-m-d H:i', $fObj->getCreatedTime())];

      $isCurrentFile = (in_array($fid, $selected_ids));

      if ($no_use) {
        $add_selection_button = FALSE;
      }
      else {
        $fileRow['activ'] = ['#markup' => $isCurrentFile ? t('Yes') : ''];
      }

      // Prepare select/unselect submit.
      if ($isCurrentFile === TRUE) {
        $link_title = t('Unselect file');
        $route_target = 'unselect_file';
      }
      else {
        if ($more_available === TRUE) {
          $link_title = t('Select file');
          $route_target = 'select_file';
        }
        else {
          $add_selection_button = FALSE;
        }
      }

      if ($add_selection_button === TRUE) {
        $operation = [
          '#name' => $element['#name'] . '_' . $route_target . '_button_' . $fid,
          '#type' => 'submit',
          '#value' => $link_title,
          '#validate' => [],
          '#submit' => ['file_history_submit'],
          '#limit_validation_errors' => [],
          '#weight' => -5,
        ];
        if ($ajax_activated === TRUE) {
          $operation['#ajax'] = $ajax_settings;
        }
        // Add select/unselect submit.
        $fileRow['operation'][] = $operation;
      }

      if (!$isCurrentFile) {

        $operation = [
          '#name' => $element['#name'] . '_delete_button_' . $fid,
          '#type' => 'submit',
          '#value' => t('Delete'),
          '#validate' => [],
          '#submit' => ['file_history_submit'],
          '#limit_validation_errors' => [],
          '#weight' => -5,
        ];

        if ($ajax_activated === TRUE) {
          $operation['#ajax'] = $ajax_settings;
        }

        // Add delete submit.
        $fileRow['operation'][] = $operation;

      }

      if (!$no_download) {
        // Add download button.
        $url = self::makeDownloadLink($fObj);
        $links = [
          'title' => t('Download'),
          'url' => $url,
        ];
        $fileRow['operation'][] = [
          'item' =>
            [
              '#type' => 'dropbutton',
              '#links' => [$links],
            ],
        ];
      }

      if (!$no_use) {
        // Add hidden boolean value.
        $fileRow['selected'] = [
          '#type' => 'hidden',
          '#value' => $isCurrentFile ? 1 : 0,
        ];
      }

      // Attach rows to table.
      $rows[$fObj->id()] = $fileRow;
    }

    // We sort files by upload time.
    krsort($rows);
    $sorted_rows = array_values($rows);

    // Wait alterations of headers.
    $field_id = isset($element['#field_general_id_for_hooks']) ? $element['#field_general_id_for_hooks'] : $element['#name'];
    // Generic alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_rows_alter',
      [&$sorted_rows, $field_id]
    );
    // Specific alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_' . $field_id . '_rows_alter',
      [&$sorted_rows]
    );

    // Make table.
    $element['table'] = [
      '#type' => 'table',
      '#header' => $header,
    ];

    // Add rows.
    foreach ($sorted_rows as $row) {
      $element['table'][] = $row;
    }

    // Return form item.
    return $element;
  }

  /**
   * Extract values of table.
   *
   * @param mixed $element
   *   Element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $new_files
   *   Files newly uploaded.
   *
   * @return array
   *   List of selected Fids.
   */
  public static function makeValues($element, FormStateInterface $form_state, array $new_files): array {

    $input = $form_state->getUserInput();
    $values = [
      'selected' => [],
      'in_table' => [],
    ];

    if (count($new_files) > 0) {
      foreach ($new_files as $fid) {
        $values['in_table'][$fid] = $fid;
      }
    }

    $in_field = _fh_get_nested_value($element['#name'], $input);

    if ($in_field != NULL && isset($in_field['table'])) {
      $table_data = $in_field['table'];

      foreach ($table_data as $row_id => $data) {

        if (isset($data['selected']) && $data['selected'] == 1) {
          $values['selected'][$data['fid']] = $data['fid'];
        }

        $values['in_table'][$data['fid']] = $data['fid'];
      }
    }
    else {
      if (isset($element["#default_value"])) {
        foreach ($element['#default_value'] as $key => $fids) {
          foreach ($fids as $fid) {
            $values[$key][$fid] = $fid;
          }
        }
      }
      else {
        $values = [
          'selected' => [],
          'in_table' => [],
        ];
      }
    }

    // Provide Legacy mode (use physical folder to manage files).
    if ($element['#legacy']) {
      /** @var \Drupal\Core\File\FileSystemInterface  $file_sytem_service */
      $file_sytem_service = \Drupal::service('file_system');

      if (!$file_sytem_service->prepareDirectory($element['#upload_location'])) {
        $file_sytem_service->prepareDirectory($element['#upload_location'], FileSystemInterface::CREATE_DIRECTORY);
        $file_sytem_service->prepareDirectory($element['#upload_location'], FileSystemInterface::MODIFY_PERMISSIONS);
      }

      $file_in_folder = $file_sytem_service->scanDirectory($element['#upload_location'], '([.*])');

      // Try loading each files.
      foreach ($file_in_folder as $physical_file) {
        $loaded = \Drupal::entityTypeManager()
          ->getStorage('file')
          ->loadByProperties(['uri' => $physical_file->uri]);
        if ($loaded) {
          $file = array_values($loaded)[0];
        }
        else {
          // If file is not know by Drupal, and autocreate mode.
          if ($element['#create_missing']) {
            $file = File::create((array) $physical_file);
            $file->save();
          }
        }

        if ($file) {
          // Add files to table.
          $values['in_table'][$file->id()] = $file->id();
        }
      }

      // If default value set, it's only selected values.
      if (isset($element["#default_value"])) {
        foreach ($element['#default_value'] as $fids => $fid) {
          $values['selected'][$fid] = $fid;
        }
      }
    }

    return $values;
  }

  /**
   * Manage select/unselect files.
   *
   * @param string $button_name
   *   Name of clicked button.
   * @param array $values
   *   Default list of Fids.
   * @param bool $is_multiple
   *   Boolean.
   */
  public static function manageSelected($button_name, array &$values, bool $is_multiple = FALSE) {

    if (strstr($button_name, '_unselect_file_button_')) {
      list($element_name, $fid) = explode('_unselect_file_button_', $button_name);

      if (isset($values['selected'][$fid])) {
        unset($values['selected'][$fid]);
      }
    }
    if (strstr($button_name, '_select_file_button_')) {
      list($element_name, $fid) = explode('_select_file_button_', $button_name);
      if ($is_multiple == TRUE) {
        $values['selected'][$fid] = $fid;
      }
      else {
        $values['selected'] = [$fid => $fid];
      }
    }

  }

  /**
   * Delete file action.
   *
   * @param string $button_name
   *   Name of button.
   * @param array $values
   *   List of values.
   */
  public static function deleteFile($button_name, array &$values) {

    if (strstr($button_name, '_delete_button_')) {
      list($element_name, $fid) = explode('_delete_button_', $button_name);

      $file = File::load($fid);
      $file->delete();

      if (isset($values['selected'][$fid])) {
        unset($values['selected'][$fid]);
      }
      if (isset($values['in_table'][$fid])) {
        unset($values['in_table'][$fid]);
      }
    }
  }

  /**
   * Generate Download URL.
   *
   * @param \Drupal\file\Entity\File $file
   *   File object.
   *
   * @return \Drupal\Core\Url
   *   Url object
   */
  public static function makeDownloadLink(File $file): Url {
    $path = $file->getFileUri();

    $scheme = \Drupal::service('file_system')->uriScheme($path);

    if ($scheme == 'public') {
      $filepath = file_create_url($path);
      return Url::fromUri($filepath);
    }
    else {
      return Url::fromRoute('file_history.download_nonpublic_file',
        [
          'file' => $file->id(),
        ]);
    }
  }

  /**
   * Render API callback: Validates the managed_file element.
   */
  public static function validateFileHistory(&$element, FormStateInterface $form_state, &$complete_form) {

    $values = $form_state->getValues();
    $output = [];
    $in_field = _fh_get_nested_value($element['#name'], $values);
    if ($in_field != NULL) {
      if ($in_field['selected'] != [] && $in_field['in_table'] != []) {
        $output = [
          'selected' => $in_field['selected'],
          'in_table' => $in_field['in_table'],
        ];
      }
    }

    $form_state->setValueForElement($element, $output);
  }

}
