<?php

namespace Drupal\file_history\Plugin\Field\FieldWidget;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'file_history_widget' widget.
 *
 * @FieldWidget(
 *   id = "file_history_widget",
 *   module = "file_history",
 *   label = @Translation("File history widget"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileHistoryWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    // Load the items for form rebuilds from the field state as they might not
    // be in $form_state->getValues() because of validation limitations. Also,
    // they are only passed in as $items when editing existing entities.
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    if (isset($field_state['items'])) {
      $items->setValue($field_state['items']);
    }

    $element = [];
    return $this->formElement($items, 0, $element, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $state_service = \Drupal::state();
    $file_history_state = $state_service->get('file_history_table', []);
    $field_id = $this->fieldDefinition->id();
    $default_table = NULL;

    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    // If Field not "know" in table_cache => we create it.
    if (!isset($file_history_state[$field_id])) {
      $file_history_state[$field_id] = [];
      $state_service->set('file_history_table', $file_history_state);
    }
    else {
      // We load table value if exists.
      $entity_id = $form_state->getFormObject()->getEntity()->id() ?? NULL;
      if ($entity_id != NULL && isset($file_history_state[$field_id][$entity_id])) {
        $entity_field_value_table = $file_history_state[$field_id][$entity_id];
        if (isset($entity_field_value_table) && isset($entity_field_value_table['in_table'])) {
          $default_table = $entity_field_value_table['in_table'];
        }
      }
    }

    // Get Values.
    $default_target = [];
    foreach ($items->getValue() as $value) {
      if (isset($value['target_id'])) {
        $default_target[] = $value['target_id'];
      }
    }

    $default = [];
    if ($default_table == NULL) {
      if (!empty($default_target)) {
        $default = [
          'selected' => $default_target,
          'in_table' => $default_target,
        ];
      }
    }
    else {
      if (!empty($default_target)) {
        $default = [
          'selected' => $default_target,
          'in_table' => $default_table,
        ];
      }
    }

    // Prepare Ids.
    $field_local_id = str_replace('_', '-', $this->fieldDefinition->getName()) . '-' . $delta;

    // Prepare file upload validators.
    // Cap the upload size according to the PHP limit.
    $max_filesize = Bytes::toInt(Environment::getUploadMaxSize());
    $max_size_setting = $this->fieldDefinition->getSetting('max_filesize');
    if ($max_size_setting != '') {
      $max_filesize = min($max_filesize, Bytes::toInt($max_size_setting));
    }
    // There is always a file size limit due to the PHP server limit.
    $validators['file_validate_size'] = [$max_filesize];

    // Add the extension check if necessary.
    $extention_settings = $this->fieldDefinition->getSetting('file_extensions');
    if ($extention_settings != '') {
      $validators['file_validate_extensions'] = [$extention_settings];
    }

    // Wait alterations of validators.
    $field_general_id_for_hooks = str_replace('.', '_', $this->fieldDefinition->id());
    // Generic alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_upload_validator_alter',
      [&$validators, $field_general_id_for_hooks]
    );
    // Specific alteration.
    \Drupal::moduleHandler()->invokeAll(
      'file_history_' . $field_general_id_for_hooks . '_upload_validator_alter',
      [&$validators]
    );

    // Prepare Upload location.
    $destination = trim($this->fieldDefinition->getSetting('file_directory'), '/');
    $uri_sheme = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('uri_scheme');

    // Replace tokens. As the tokens might contain HTML we convert it to plain
    // text.
    $destination = PlainTextOutput::renderFromHtml(\Drupal::token()->replace($destination));
    $upload_location = $uri_sheme . '://' . $destination;

    // Make Widget.
    $element += [
      '#field_general_id_for_hooks' => $field_general_id_for_hooks,
      '#type' => 'file_history',
      '#title' => $this->fieldDefinition->getLabel(),
      '#size' => 50,
      // Setting an id for the wrapper will activate ajax on buttons.
      '#theme_wrappers' => [
        'form_element' => [
          '#wrapper_attributes' => ['id' => $field_local_id],
        ],
      ],
      // Like Managed Files, general file validation.
      '#upload_validators' => $validators,
      // Folder to store files.
      '#upload_location' => $upload_location,
      // If you need validation content of files before store it.
      // If folder contain file not knowed by Drupal, we save they.
      '#create_missing' => TRUE,
      '#field_name' => $this->fieldDefinition->getName(),
      '#cardinality' => $cardinality,
      '#extended' => TRUE,
      '#default_value' => $default,
    ];

    // Set flag for form submit.
    if (!isset($form['file_history_after_build'])) {
      $form['#file_history_after_build'] = TRUE;
    }

    // Set description.
    $desc = [];
    if ($this->fieldDefinition->getDescription() == '') {
      $desc[] = $this->t('List of files');
    }
    else {
      $desc[] = $this->fieldDefinition->getDescription();
    }
    if ($cardinality == -1) {
      $desc[] = t('Unlimited number of files can be selected to this field.');
    }
    else {
      $desc[] = \Drupal::translation()->formatPlural($cardinality, 'One selected file only.', 'Maximum @count selected files.');
    }
    $file_upload_help = [
      '#theme' => 'file_upload_help',
      '#upload_validators' => $validators,
    ];
    $desc[] = \Drupal::service('renderer')->renderPlain($file_upload_help);

    $element['#description'] = implode('<br />', $desc);

    // Return Widget.
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    // Since file upload widget now supports uploads of more than one file at a
    // time it always returns an array of fids. We have to translate this to a
    // single fid, as field expects single value.
    $new_values = [];
    foreach ($values as $value) {
      if ($value['_original_delta'] == 'selected') {
        unset($value['_original_delta']);
        foreach ($value as $fid) {
          $new_value = [];
          $new_value['target_id'] = $fid;
          $new_values[] = $new_value;
        }
      }
    }

    return $new_values;
  }

}
