<?php

namespace Drupal\file_history\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Drupal\Core\File\FileSystem;

/**
 * Class FileHistoryDownloadController.
 */
class FileHistoryDownloadController extends ControllerBase {

  /**
   * File_system_service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  private $fileSystemService;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystem $file_system_service) {
    $this->fileSystemService = $file_system_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    /** @var /Drupal/Core/File/FileSystem $file_system_service */
    $file_system_service = $container->get('file_system');

    return new static($file_system_service);
  }

  /**
   * Download non public files.
   *
   * @param \Drupal\file\FileInterface $file
   *   File object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   File content
   */
  public function downloadFile(FileInterface $file) {

    $real_path = $this->fileSystemService->realpath($file->getFileUri());
    $fileContent = file_get_contents($real_path);

    $response = new Response($fileContent);

    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getFilename()
    );

    $response->headers->set('Content-Disposition', $disposition);

    return $response;
  }

}
