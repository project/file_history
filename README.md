# Keep history of my custom uploaded files

## 1. Purpose

File_history offer a new form_api field type permiting :
- The upload of a file in a specific folder
- A validation between the serveur upload and drupal saving of file
- The list all files uploaded in this folder
- The activation of a specific file in this collection

The use case at the origin of this field are a tree like decision tool.
This tree are manage by a huge matrix datafile, and webmaster need to be
available to update the data pool.
The memory of old uploaded files came to avaoid loosing a good data pool
by the upload of a malformed matrix data file.

A full implementation exemple form are present in src/Form folder
( web path : /admin/file_history/exemple_form ).

## 2. Implementation

The field use is similar than managed_file field

```
$form['configurations_files'] = [
        '#type' => 'file_history',
        '#title' => $this->t('Configurations'),
        '#description' => $this->t('List of files'),
        '#size' => 50,
        //Setting an id for the wrapper will activate ajax on buttons
        '#theme_wrappers' => [
          'form_element' =>[
            '#wrapper_attributes' => ['id' => 'change-this-id-wrapper'],
          ]
        ],
        // Like Managed Files, general file validation.
        '#upload_validators' => $validators,
        // Folder to store files.
        '#upload_location' => 'public://my_configuration/',

        // Default Values : list of Fids
        '#default_values' => $fids,
        // Table authorise multiple selection ?
        '#multiple' => FALSE

        // TRUE if you want desactivate upload fields
        '#no_upload' => FALSE,
        // TRUE if you want desactivate "use" action
        '#no_use' => FALSE,
        // TRUE if you want desactivate "download file" action
        '#no_download' => TRUE,

    ];
```

The ``create_missing`` item permit to autorize the field to save in Drupal the
files present in the folder which not exist in in Drupal.
That can be use full if you folder received files from a another source than
Drupal like a shell script.


## 4. Visual

![ExempleForm.PNG](ExempleForm.PNG)

The field visual are composed in one part with a upload field and a submit.
The second part are a table listing all file already load in
``upload_location``.
Each file have operations.
The chosen file make be reload, this call a Controler which behavior will
be alterable soon, permitting manipulation of file.
A non chosen make be choose or deleted.

On the form submit action, the field value is a array with state of the two
 html fields (upload and button) plus the value of the chosen file in the
 'selected_file' attribut.

 See a exemple of implementation below.
```
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $value = $form_state->getValue('configurations_files');

    // Do something on submit.

    $this->config('exemple_file_history.default')
      ->set('selected_configuration_file', $value['selected_file'])
      ->save();
  }
```
