<?php

/**
 * @file
 * Hooks for file_history module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter Upload validators for file_history widget on all fields.
 *
 * @param mixed $validators
 *   List of file validators @see hook_file_validate()
 * @param string $field_id
 *   Field ID.
 */
function hook_file_history_upload_validator_alter(&$validators, $field_id) {
  if ($field_id == 'my_field') {
    $validators['my_custom_validator'] = ['my_custom parameter'];
  }
}

/**
 * Alter Upload validators for a specific file_history widget.
 *
 * @param mixed $validators
 *   List of file validators @see hook_file_validate()
 */
function hook_file_history_FIELD_ID_upload_validator_alter(&$validators) {
  $validators['my_custom_validator'] = ['my_custom parameter'];
}

/**
 * Alter Table Header for all form elements.
 *
 * @param mixed $headers
 *   List of file table headers.
 * @param string $field_id
 *   Field ID.
 */
function hook_file_history_headers_alter(&$headers, $field_id) {
  if ($field_id == 'my_field') {
    $headers['file_type'] = t('File type');
  }

}

/**
 * Alter Table Header for a specific file_history form element.
 *
 * @param mixed $headers
 *   List of file table headers.
 */
function hook_file_history_FIELD_ID_headers_alter(&$headers) {

  $headers['file_type'] = t('File type');

}

/**
 * Alter Table Row for all file_history form elements.
 *
 * @param mixed $rows
 *   List of file table rows.
 * @param string $field_id
 *   Field ID.
 */
function hook_file_history_rows_alter(&$rows, $field_id) {

  if ($field_id == 'my_field') {
    $select = [
      '#type' => 'select',
      '#title' => t('Select Language of data'),
      '#options' => [
        'commercial' => 'Commercial Communication',
        'financial' => 'Financial Communication',
      ],
    ];

    $values = \Drupal::state()->get('magic_memory', []);

    foreach ($rows as $row_key => $row_data) {

      $select['#default_value'] = isset($values[$row_key]) ? $values[$row_key] : 'commercial';
      $rows[$row_key]['file_type'] = $select;

    }
  }

}

/**
 * Alter Table Row for a specific file_history form element.
 *
 * @param mixed $rows
 *   List of file table rows.
 */
function hook_file_history_FIELD_ID_rows_alter(&$rows) {

  $select = [
    '#type' => 'select',
    '#title' => t('Select Language of data'),
    '#options' => [
      'commercial' => 'Commercial Communication',
      'financial' => 'Financial Communication',
    ],
  ];

  $values = \Drupal::state()->get('magic_memory', []);

  foreach ($rows as $row_key => $row_data) {

    $select['#default_value'] = isset($values[$row_key]) ? $values[$row_key] : 'commercial';
    $rows[$row_key]['file_type'] = $select;

  }

}

/**
 * @} End of "addtogroup hooks".
 */
