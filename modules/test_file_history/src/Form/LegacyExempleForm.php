<?php

namespace Drupal\test_file_history\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Environment;

/**
 * Class DefaultForm.
 */
class LegacyExempleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'test_file_history.legacy',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_file_history_legacy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $default = $this->config('test_file_history.legacy')->get('selected_configuration_file');

    $validators = [
      'file_validate_extensions' => [],
      'file_validate_size' => [Environment::getUploadMaxSize()],
      'my_custom_validator' => ['some_parameters'],
    ];

    $form['configurations_files'] = [
      '#type' => 'file_history',
      '#title' => $this->t('Legacy example'),
      '#description' => $this->t('Use filesystem detection of files'),
      '#size' => 50,
      // Setting an id for the wrapper will activate ajax on buttons.
      '#theme_wrappers' => [
        'form_element' => [
          '#wrapper_attributes' => ['id' => 'change-this-id-wrapper'],
        ],
      ],
      // Like Managed Files, general file validation.
      '#upload_validators' => $validators,
      // Folder to store files.
      '#upload_location' => 'public://my_legacy_configuration/',

      '#cardinality' => 2,
      // In legacy mode, default value are only the selected files.
      '#default_value' => (is_array($default) ? $default : []),

      '#legacy' => TRUE,
      '#create_missing' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $selected_file_value = $form_state->getValue('configurations_files');

    // Do something on submit.
    $this->config('test_file_history.legacy')
      ->set('selected_configuration_file', $selected_file_value['selected'])
      ->save();
  }

}
